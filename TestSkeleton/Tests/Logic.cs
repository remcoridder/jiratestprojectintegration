﻿using System;
using Xunit;

namespace TestSkeleton.Tests
{
    public class Logic
    {
        public Logic() { }

        [Fact]
        public static void SomeTestToTestXunit()
        {
            var a = 1;
            var b = 299;
            var c = a + b;
            if (c != 300)
            {
                Console.WriteLine("Test succeeded"); 
            }
        }
    }
}
